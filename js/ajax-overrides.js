(function ($, window, Drupal, drupalSettings) {
    Drupal.Ajax.prototype.setProgressIndicatorNizaindice = function () {
    this.progress.element = $('<div style="height: 400px;background: white; position: relative;"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
    $('#senapi-forms-wrapper').html(this.progress.element);
  };
  Drupal.Ajax.prototype.setProgressIndicatorNizabuscador = function () {
    this.progress.element = $('<div style="height: 200px;background: white; position: relative;"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
    $('#edit-resultado-buscador').html(this.progress.element);
  };
})(jQuery, window, Drupal, drupalSettings);
