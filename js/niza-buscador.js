(function ($, Drupal) {
  Drupal.behaviors.senapi_forms = {
    attach: function (context) {
      var products = [], services = [], all = [];
      $('input[name^="indice"]', context).once('input[name^="indice"]').each(function () {
        if ($(this).val() <= 34) {
          products.push($(this));
        } else if ($(this).val() > 34) {
          services.push($(this));
        }
        all.push($(this));
      });
      $('input[name=clases]', context).once('input[name=clases]').each(function () {
        $(this).change(function () {
          switch ($(this).val()) {
            case 'PROD':
              services.forEach(function (item) {
                $(item).prop('checked', false);
              });
              products.forEach(function (item) {
                $(item).prop('checked', true);
              });
              break;
            case 'SERV':
              products.forEach(function (item) {
                $(item).prop('checked', false);
              });
              services.forEach(function (item) {
                $(item).prop('checked', true);
              });
              break;
            case 'ALL':
              all.forEach(function (item) {
                $(item).prop('checked', true);
              });
              break;
            case 'NONE':
              all.forEach(function (item) {
                $(item).prop('checked', false);
              });
              break;
          }
        });
        if ($(this).is(':checked')) {
          $(this).trigger('change');
        }
      });
    }
  };
})(jQuery, Drupal);
