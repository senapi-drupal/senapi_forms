(function ($, Drupal) {
  Drupal.behaviors.senapi_forms = {
    attach: function (context) {
      $('#notification', context).once('#notification').each(function () {
        var dtOptions = {
          search: {
            regex: true,
          },
          autoWidth: false,
          columns: [{
            title: 'N°',
            defaultContent: null,
            render: function (data, type, row, meta) {
              return meta.row + 1;
            },
          }, {
            data: 'tramite',
            title: 'Trámite',
            width: "30%",
            createdCell:  function (td) {
              $(td).attr('style', 'min-width: 150px');
            }
          }, {
            data: 'fechaEnvio',
            title: 'Fecha de envío',
            width: "30%",
            createdCell:  function (td) {
              $(td).attr('style', 'min-width: 180px');
            }
          }/*, {
            data: 'nombre',
            title: 'Nombre',
            width: "50%",
            createdCell:  function (td) {
              $(td).attr('style', 'min-width: 300px');
            }
          }*/, {
            title: 'Acciones',
            defaultContent: null,
            render: function (data, type, row, meta) {
              return '<button class="action-show btn btn-primary btn-sm"  type="button" title="Ver Documento"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>';
            }

          }],

          ajax: '/api/notifications',
          language: {
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            emptyTable: "Ningún dato disponible en esta tabla",
            info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
            infoFiltered: "(filtrado de un total de _MAX_ registros)",
            infoPostFix: "",
            search: "Busca:",
            url: "",
            infoThousands: ",",
            loadingRecords: "Cargando...",
            paginate: {
              first: "«",
              last: "»",
              next: "›",
              previous: "‹"
            },
            aria: {
              sortAscending: ": Activar para ordenar la columna de manera ascendente",
              sortDescending: ": Activar para ordenar la columna de manera descendente"
            }
          },
          pagingType: 'full_numbers',
          responsive: false,
          dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>" +
            "<'row'<'col-sm-12'<'table-responsive-lg' tr>>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",

          drawCallback: function () {

          },
          rowCallback: function (row, data, index) {
            $('.action-show', row).off('click');
            $('.action-show', row).on('click', function () {
              $('.bd-example-modal-lg').modal({show: true, keyboard: true});
            });
          },
          initComplete: function (settings, json) {

          }

        };

        var dt = $(this).DataTable(dtOptions);
      });
    }
  };
})(jQuery, Drupal);
