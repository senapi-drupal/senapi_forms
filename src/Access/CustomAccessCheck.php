<?php
namespace Drupal\senapi_forms\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Access\AccessResult;

class CustomAccessCheck implements AccessInterface {

  public static function access(AccountInterface $account) {
       return (in_array(\Drupal::request()->getClientIp(), ['::1', '127.0.0.1','192.168.6.57', '192.168.6.95', '192.168.139.252'])) ? AccessResult::allowed() : AccessResult::forbidden();
  }
}
