<?php
/**
 * Created by PhpStorm.
 * User: senapi
 * Date: 22-02-19
 * Time: 03:10 PM
 */

namespace Drupal\senapi_forms\Controller;


use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

class ContactoController extends ControllerBase {

  public function contactoForms(Request $request) {
    $session = $request->getSession();
    $formName = 'contacto';
    if ($session->get('form')) {
      $formName = $session->get('form');
      $session->remove('form');
    }

    $formContacto = $this->formBuilder()
      ->getForm('Drupal\senapi_forms\Form\ContactoForm');
    $formDenuncia = $this->formBuilder()
      ->getForm('Drupal\senapi_forms\Form\DenunciaForm');
    $formQueja = $this->formBuilder()
      ->getForm('Drupal\senapi_forms\Form\QuejaForm');
    $formSugerencia = $this->formBuilder()
      ->getForm('Drupal\senapi_forms\Form\SugerenciaForm');

    $render = \Drupal::service('renderer');
    $htmlContacto = $render->render($formContacto);
    $htmlDenuncia = $render->render($formDenuncia);
    $htmlQueja = $render->render($formQueja);
    $htmlSugerencia = $render->render($formSugerencia);


    return [
      '#type' => 'inline_template',
      '#template' => '
         <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link {{ tabActive.contacto }}" id="nav-contacto-tab" data-toggle="tab" href="#nav-contacto" role="tab" aria-controls="nav-contacto" aria-selected="true">Contacto</a>
              <a class="nav-item nav-link {{ tabActive.denuncia }}" id="nav-denuncia-tab" data-toggle="tab" href="#nav-denuncia" role="tab" aria-controls="nav-denuncia" aria-selected="false">Denuncia</a>
              <a class="nav-item nav-link {{ tabActive.queja }}" id="nav-queja-tab" data-toggle="tab" href="#nav-queja" role="tab" aria-controls="nav-queja" aria-selected="false">Queja</a>
              <a class="nav-item nav-link {{ tabActive.sugerencia }}" id="nav-sugerencia-tab" data-toggle="tab" href="#nav-sugerencia" role="tab" aria-controls="nav-sugerencia" aria-selected="false">Sugerencia</a>
            </div>
         </nav>
         <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade {{ tabActive.contacto }}" id="nav-contacto" role="tabpanel" aria-labelledby="nav-contacto-tab">{{htmlContacto}}</div>
            <div class="tab-pane fade {{ tabActive.denuncia }}" id="nav-denuncia" role="tabpanel" aria-labelledby="nav-denuncia-tab">{{htmlDenuncia}}</div>
            <div class="tab-pane fade {{ tabActive.queja }}" id="nav-queja" role="tabpanel" aria-labelledby="nav-queja-tab">{{htmlQueja}}</div>
            <div class="tab-pane fade {{ tabActive.sugerencia }}" id="nav-sugerencia" role="tabpanel" aria-labelledby="nav-sugerencia-tab">{{htmlSugerencia}}</div>
         </div>',
      '#context' => [
        'htmlContacto' => $htmlContacto,
        'htmlDenuncia' => $htmlDenuncia,
        'htmlQueja' => $htmlQueja,
        'htmlSugerencia' => $htmlSugerencia,
        'tabActive' => [
          'contacto' => ($formName == 'contacto') ? 'show active' : '',
          'denuncia' => ($formName == 'denuncia') ? 'show active' : '',
          'queja' => ($formName == 'queja') ? 'show active' : '',
          'sugerencia' => ($formName == 'sugerencia') ? 'show active' : '',
        ],
      ],
    ];
  }

}