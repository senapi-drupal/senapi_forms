<?php


namespace Drupal\senapi_forms\Controller;


use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\JsonResponse;

class JsonApiController {

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function cgetNotification() {
      return $this->getNotificationResults();
  }

  private function getNotificationResults() {
    //PROXY PASS NOTIFICATION
    // https://stackoverflow.com/questions/19748105/handle-guzzle-exception-and-get-http-body/30957410
    // https://www.drupal.org/docs/8/core/modules/rest/2-get-for-reading-content-entities
    try {
      $response = \Drupal::httpClient()
        ->get('https://apidevnotificacion.senapi.gob.bo/api/pagina/manual', [
          'headers' => [
            "Authorization" => "Bearer " . getenv('NOTIFICATION_API'),
          ],
        ]);
      $data = (string) $response->getBody();
      return new JsonResponse(["data" => Json::decode($data)]);
    } catch (ClientException $e) {
      if ($e->hasResponse()) {
        $exception = Json::decode((string) $e->getResponse()->getBody());
        return new JsonResponse($exception, $e->getCode());
      } else {
        return new JsonResponse($e->getMessage(), 503);
      }
    } catch (RequestException $e) {
      if ($e->hasResponse()) {
        $exception = Json::decode((string) $e->getResponse()->getBody());
        return new JsonResponse($exception, $e->getCode());
      } else {
        return new JsonResponse($e->getMessage(), 503);
      }
    }
  }
}
