<?php

namespace Drupal\senapi_forms\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;


class HandlerFile {

  public static function process($element, FormStateInterface $form_state, $form) {
    if (!empty($element['#files'])) {
      $element['description']['#required'] = TRUE;

      switch ($form['#form_id']) {
        case 'node_gaceta_form':
        case 'node_gaceta_edit_form':
          switch ($element['#field_name']) {
            case 'field_file':
              if (empty($element['description']['#value'])) {
                $element['description']['#value'] = 'Signos';
              }
              break;
            case 'field_file_second':
              if (empty($element['description']['#value'])) {
                $element['description']['#value'] = 'Patentes';
              }
              break;
            case 'field_file_third':
              if (empty($element['description']['#value'])) {
                $element['description']['#value'] = 'D. Autor';
              }
              break;
          }
          break;
        case 'node_oposicion_form':
        case 'node_oposicion_edit_form':
          switch ($element['#field_name']) {
            case 'field_file':
              if (empty($element['description']['#value'])) {
                $element['description']['#value'] = 'Lista de Oposiciones (Signos Distintivos)';
              }
              $element['description']['#attributes']['readonly'] = TRUE;
              break;
            case 'field_file_second':
              if (empty($element['description']['#value'])) {
                $element['description']['#value'] = 'Lista de Oposiciones (Patentes)';
              }
              $element['description']['#attributes']['readonly'] = TRUE;
              break;
          }
          break;

        case 'node_boletin_form':
        case 'node_boletin_edit_form':
          switch ($element['#field_name']) {
            case 'field_file':
              if (empty($element['description']['#value'])) {
                $element['description']['#value'] = 'generado automaticamente.';
              }
              break;
          }
          break;
        case 'node_revista_form':
        case 'node_revista_edit_form':
          switch ($element['#field_name']) {
            case 'field_file':
              if (empty($element['description']['#value'])) {
                $element['description']['#value'] = 'generado automaticamente.';
              }
              break;
            case 'field_image':
              if (empty($element['alt']['#default_value'])) {
                $element['alt']['#default_value'] = 'generado automaticamente.';
              }
              break;
          }
          break;

        case 'node_auditoria_form':
        case 'node_auditoria_edit_form':
          switch ($element['#field_name']) {
            case 'field_file':
              if (empty($element['description']['#value'])) {
                $element['description']['#value'] = 'Resumen Ejecutivo 00 0000';
              }
              break;
          }
          break;

        case 'node_prensa_form':
        case 'node_prensa_edit_form':
        case 'node_fondo_form':
        case 'node_fondo_edit_form':
        case 'node_fondopagina_form':
        case 'node_fondopagina_edit_form':
          switch ($element['#field_name']) {
            case 'field_image':
            case 'field_images':
              if (empty($element['alt']['#default_value'])) {
                $element['alt']['#default_value'] = 'alt generado automaticamente.';
              }
              $element['alt']['#attributes']['readonly'] = TRUE;
              if (empty($element['title']['#default_value'])) {
                $element['title']['#default_value'] = 'title generado automaticamente.';
              }
              $element['title']['#attributes']['readonly'] = TRUE;
              break;
          }
          break;
      }

    }
    return $element;
  }

  public static function submitHandle(array &$form, FormStateInterface $form_state) {
    $formId = $form_state->getValue('form_id');
    switch ($formId) {
      case 'node_gaceta_edit_form':
      case 'node_gaceta_form':
        $gaceta = $form_state->getValue(['field_number', 0])['value'];
        $newTitle = 'GACETA OFICIAL DE BOLIVIA NRO. ' . $gaceta;
        $title = $form_state->getValue(['title', 0])['value'];
        if ($title != $newTitle) {
          $form_state->setValue('title', [['value' => $newTitle]]);
        }
        break;

      case 'node_oposicion_edit_form':
      case 'node_oposicion_form':
        $gacetaId = $form_state->getValue(['field_reference', 0])['target_id'];
        $gaceta = Node::load($gacetaId)
          ->get('field_number')
          ->first()
          ->getValue()['value'];
        $newTitle = 'OPOSICIÓN GACETA NRO. ' . $gaceta;

        $nodes = \Drupal::entityQuery('node')
          ->condition('type', 'oposicion')
          ->condition('title', $newTitle, 'IN')
          ->execute();

        if (!empty($nodes) && $formId == 'node_oposicion_form') {
          $newTitle = $newTitle . '/' . (count($nodes) + 1);
        }

        $title = $form_state->getValue(['title', 0])['value'];
        if (!empty($gaceta) && $title != $newTitle) {
          $form_state->setValue('title', [['value' => $newTitle]]);
        }
        break;

      case 'node_prensa_form':
      case 'node_prensa_edit_form':
        $title = $form_state->getValue(['title', 0])['value'];
        $datePub = $form_state->getValue(['created', 0])['value'];
        $fieldImages = $form_state->getValue(['field_images']);

        $date = new \DateTime($datePub);
        $nameDate = $date->format('Ymd');

        $data = [];
        $data[] = 'prensa';
        $data[] = $nameDate;

        foreach ($fieldImages as $keyImage => &$fieldImage) {
          if (!empty($fieldImage['fids'])) {
            $i = $keyImage;
            $number = str_pad(++$i, 2, 0, STR_PAD_LEFT);
            $imageData = $data;
            $imageData[] = $number;
            $imageData[] = $title;

            $nameAlias = mb_strtoupper(join(" ", $imageData));
            $fieldImage['alt'] = $nameAlias;
            $fieldImage['title'] = $nameAlias;
          }
        }
        $form_state->setValue('field_images', $fieldImages);
        break;

      case 'node_fondo_form':
      case 'node_fondo_edit_form':
      case 'node_fondopagina_form':
      case 'node_fondopagina_edit_form':
        $title = $form_state->getValue(['title', 0])['value'];
        $fieldImages = $form_state->getValue(['field_image']);

        foreach ($fieldImages as $keyImage => &$fieldImage) {
          if (!empty($fieldImage['fids'])) {
            $fieldImage['alt'] = $title;
            $fieldImage['title'] = $title;
          }
        }
        $form_state->setValue('field_image', $fieldImages);
        break;
    }
  }

  public static function renameFile(array &$form, FormStateInterface $form_state) {
    $title = $form_state->getValue(['title', 0])['value'];
    $created = $form_state->getValue(['created', 0])['value'];
    $date = new \DateTime($created);
    $nameDate = $date->format('Ymd');

    $formId = $form['#form_id'];
    list($node, $contentType) = explode('_', $formId);
    switch ($formId) {
      case 'node_gaceta_edit_form':
      case 'node_gaceta_form':
        $fieldNameFirst = 'field_file';
        $fieldNameSecond = 'field_file_second';
        $fieldNameThird = 'field_file_third';
        $signoFile = $form_state->getValue([$fieldNameFirst, 0]);
        $patenteFile = $form_state->getValue([$fieldNameSecond, 0]);
        $dautorFile = $form_state->getValue([$fieldNameThird, 0]);

        $gacetaNumber = $form_state->getValue(['field_number', 0]);
        $gacetaDate = $form_state->getValue(['created', 0]);

        $data = [];
        $data[] = 'gaceta';
        $data[] = $gacetaNumber['value'];
        $date = new \DateTime($gacetaDate['value']);
        if (!empty($signoFile['fids'])) {
          $title = 'Signos';
          if (preg_grep("/^tom+.*$/i", [trim(strtolower($signoFile['description']))])) {
            $title = 'Tomo';
          }

          @self::source($form, $fieldNameFirst, $form_state->getValue([$fieldNameFirst]), $data, [
            'number' => FALSE,
            $title,
            $date->format('Ymd'),
          ]);
        }

        if (!empty($patenteFile['fids'][0])) {
          $title = 'Patentes';
          if (preg_grep("/^tom+.*$/i", [trim(strtolower($patenteFile['description']))])) {
            $title = 'Tomo';
          }

          @self::source($form, $fieldNameSecond, $form_state->getValue([$fieldNameSecond]), $data, [
            'number' => FALSE,
            $title,
            $date->format('Ymd'),
          ]);
        }

        if (!empty($dautorFile['fids'][0])) {
          $title = 'Dautor';
          if (preg_grep("/^tom+.*$/i", [trim(strtolower($dautorFile['description']))])) {
            $title = 'Tomo';
          }

          @self::source($form, $fieldNameThird, $form_state->getValue([$fieldNameThird]), $data, [
            'number' => FALSE,
            $title,
            $date->format('Ymd'),
          ]);
        }
        break;

      case 'node_oposicion_edit_form':
      case 'node_oposicion_form':
        $gacetaId = $form_state->getValue(['field_reference', 0])['target_id'];
        $node = Node::load($gacetaId);
        $gaceta = $node->get('field_number')
          ->first()
          ->getValue()['value'];
        $gacetaDate = $node->get('created')
          ->first()
          ->getValue()['value'];

        $fieldNameFirst = 'field_file';
        $fieldNameSecond = 'field_file_second';
        $signoFile = $form_state->getValue([$fieldNameFirst]);
        $patenteFile = $form_state->getValue([$fieldNameSecond]);

        $data = [];
        $data[] = 'oposicion';
        $data[] = 'gaceta';
        $data[] = $gaceta;

        $created = date('Ymd', $gacetaDate);

        @self::source($form, $fieldNameFirst, $signoFile, $data, [
          'number' => FALSE,
          'signo',
          $created,
        ]);

        @self::source($form, $fieldNameSecond, $patenteFile, $data, [
          'number' => FALSE,
          'patente',
          $created,
        ]);

        break;

      case 'node_boletin_form':
      case 'node_boletin_edit_form':
      case 'node_revista_form':
      case 'node_revista_edit_form':
        $fieldNameImage = 'field_image';
        $fieldNameFile = 'field_file';
        $fieldFile = $form_state->getValue([$fieldNameFile]);
        $fieldImage = $form_state->getValue([$fieldNameImage]);

        $data = [];
        $data[] = $contentType;
        $data[] = $nameDate;

        @self::source($form, $fieldNameImage, $fieldImage, $data, [$title]);

        @self::source($form, $fieldNameFile, $fieldFile, $data, [$title]);
        break;

      case 'node_auditoria_form':
      case 'node_auditoria_edit_form':
        $fieldName = 'field_file';
        $data = [];
        $data[] = $contentType;
        $data[] = $nameDate;

        @self::source($form, $fieldName, $form_state->getValue([$fieldName]), $data, [
          $form_state->getValue([
            'field_file',
            0,
          ])['description'],
        ]);
        break;

      case 'node_prensa_form':
      case 'node_prensa_edit_form':
        $fieldName = 'field_images';
        $fieldImages = $form_state->getValue([$fieldName]);

        $data = [];
        $data[] = $contentType;
        $data[] = $nameDate;

        @self::source($form, $fieldName, $fieldImages, $data, [$title]);
        break;

      case 'node_media_form':
      case 'node_media_edit_form':
        $fieldName = 'field_file';
        $mediaFile = $form_state->getValue([$fieldName]);
        $data = [];
        $data[] = $contentType;
        $data[] = $date->format('YmdHis');

        @self::source($form, $fieldName, $mediaFile, $data, [$title]);
        break;

      case 'node_fondo_form':
      case 'node_fondo_edit_form':
      case 'node_fondopagina_form':
      case 'node_fondopagina_edit_form':
        $fieldName = 'field_image';
        $fieldImages = $form_state->getValue([$fieldName]);
        $data = [];
        $data[] = $contentType;
        $data[] = $nameDate;

        @self::source($form, $fieldName, $fieldImages, $data, [$title]);
        break;
    }
  }

  public static function source($form, $fieldName, $sources, $data, $params = []) {
    foreach ($sources as $keySource => $fieldSource) {
      if (!empty($fieldSource['fids'])) {
        $i = $keySource;
        $sourceData = $data;
        $params2 = $params;
        if (!isset($params2['number'])) {
          $sourceData[] = str_pad(++$i, 2, 0, STR_PAD_LEFT);
        }
        unset($params2['number']);

        foreach ($params2 as $paramValue) {
          $sourceData[] = $paramValue;
        }

        $nameAlias = \Drupal::service('pathauto.alias_cleaner')
          ->cleanString(join(" ", $sourceData));

        $file = File::load($fieldSource['fids'][0]);

        $fileName = join('.', [
          $nameAlias,
          pathinfo($file->getFilename(), PATHINFO_EXTENSION),
        ]);

        if ($file->getFilename() != $fileName) {
          $uploadLocation = $form[$fieldName]['widget'][$keySource]['#upload_location'];
          $destination = $uploadLocation . '/' . $fileName;

          $file = file_move($file, $destination);
          @self::exiftool(join(" ", $sourceData), \Drupal::service('file_system')
            ->realpath($file->getFileUri()));
        }
      }
    }
  }

  public static function exiftool($title, $file) {
    $creator = 'Servicio Nacional de Propiedad Intelectual';
    $command = sprintf('exiftool -r -overwrite_original -P -all:all= -Creator=%s -Title=%s %s', escapeshellarg($creator), escapeshellarg(mb_strtoupper($title)), escapeshellarg($file));

    $output = [];
    $return = NULL;
    exec($command, $output, $return);
  }

}
