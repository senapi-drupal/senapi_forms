<?php

namespace Drupal\senapi_forms\Form;

use Drupal\Component\Utility\EmailValidator;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\senapi_forms\FormHelper;

class ContactoForm extends FormBase {

  protected $formHelper;

  protected $emailValidator;

  public function __construct(FormHelper $formHelper, EmailValidator $emailValidator) {
    $this->formHelper = $formHelper;
    $this->emailValidator = $emailValidator;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('senapi_forms.form_helper'),
      $container->get('email.validator')
    );
  }

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'senapi_forms_contacto_form';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>Usted puede CONTACTARSE con el SENAPI para expresar cualquier solicitud, interés o duda.<br>
                    Los campos marcados con asterisco (*) son OBLIGATORIOS.</p>'),
    ];

    $form['row_group'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'form-row',
        ],
      ],
    ];

    $form['row_group']['nombre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre y Apellido'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#wrapper_attributes' => ['class' => ['col-md-6']],

    ];

    $form['row_group']['correo'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#wrapper_attributes' => ['class' => ['col-md-6']],
    ];

    $form['row_group2'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'form-row',
        ],
      ],
    ];


    $form['row_group2']['pais'] = [
      '#type' => 'select',
      '#title' => $this->t('País'),
      '#required' => TRUE,
      '#options' => $this->formHelper->getDominio('pais'),
      '#attributes' => [
        'class' => ['form-control'],
      ],
      '#wrapper_attributes' => ['class' => ['col-md-6']],
    ];

    $form['row_group2']['departamento'] = [
      '#type' => 'select',
      '#title' => $this->t('Departamento'),
      '#required' => FALSE,
      '#options' => $this->formHelper->getDominio('departamento'),
      '#attributes' => [
        'class' => ['form-control'],
      ],
      '#wrapper_attributes' => ['class' => ['col-md-6']],
      '#states' => [
        'visible' => [
          ':input[name="pais"]' => [
            ['value' => 'BO'],
          ],
        ],
      ],
    ];

    $form['descripcion'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Comentario'),
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#attributes' => ['class' => ['ml-0 mr-0']],
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $pais = $form_state->getValue('pais');
    switch ($pais) {
      case 'BO':
        if (!in_array($form_state->getValue('departamento'), array_keys($this->formHelper->getDominio('departamento')))) {
          $form_state->setErrorByName('departamento', 'Departamento valor incorrecto.');
        }
        break;
      default:
        if (!in_array($pais, array_keys($this->formHelper->getDominio('pais')))) {
          $form_state->setErrorByName('pais', 'País valor incorrecto.');
        }
        break;
    }

    $correo = trim($form_state->getValue('correo'));
    if (!$this->emailValidator->isValid($correo)) {
      $form_state->setErrorByName('correo', $this->t('@correo es una direción de correo electrónico no válida', ['@correo' => $correo]));
    }
  }


  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data = $form_state->getUserInput();

    foreach ($form_state->getCleanValueKeys() as $item) {
      unset($data[$item]);
    }

    $this->formHelper->parseFormData($data);

    $data['tipo_formulario'] = 'CONT';

    $connection = \Drupal::database();
    try {
      $id = $connection->insert('snp_transparencia')
        ->fields($data)
        ->execute();

      $pais = $this->formHelper->getDominio('pais', [':codigo' => $data['pais']]);
      $data['pais'] = $pais[$data['pais']];

      if (isset($data['departamento'])){
        $departamento = $this->formHelper->getDominio('departamento', [':codigo' => $data['departamento']]);
        $data['departamento'] = $departamento[$data['departamento']];
      }

      $tipoFormulario = $this->formHelper->getDominio('tipo_formulario', [':codigo' => $data['tipo_formulario']]);
      $data['formulario'] = $tipoFormulario[$data['tipo_formulario']];

      $data['id'] = $id;

      $result = \Drupal::service('plugin.manager.mail')
        ->mail('swiftmailer', 'transparencia', $form_state->getValue(['correo']), \Drupal::languageManager()
          ->getDefaultLanguage()
          ->getId(), $params = ['data' => $data], $reply = NULL);
      if ($result['result']) {
        $connection
          ->update('snp_transparencia')
          ->fields([
            'usuario_estado' => 'ENV',
          ])
          ->condition('id', $id)
          ->execute();
      }

      $result = \Drupal::service('plugin.manager.mail')
        ->mail('swiftmailer', '_transparencia', 'plataforma@senapi.gob.bo', \Drupal::languageManager()
          ->getDefaultLanguage()
          ->getId(), $params = ['data' => $data], $reply = NULL);
      if ($result['result']) {
        $connection
          ->update('snp_transparencia')
          ->fields([
            'funcionario_estado' => 'ENV',
          ])
          ->condition('id', $id)
          ->execute();
      }

      \Drupal::messenger()
        ->addMessage("La acción ha sido completada satisfactoriamente, muchas gracias por usar este servicio.");
      #exit(0);
    } catch (\Exception $e) {
      switch ($e->getCode()) {
        case 23505:
          \Drupal::messenger()
            ->addWarning("El mensaje ha sido enviado.");
          break;
        default:
          \Drupal::messenger()
            ->addError("Error al guardar los datos, intente mas tarde.");
          break;
      }
    }

  }
}