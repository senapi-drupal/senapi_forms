<?php

namespace Drupal\senapi_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\senapi_forms\FormHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;


class DenunciaForm extends FormBase {

  protected $formHelper;

  public function __construct(FormHelper $formHelper) {
    $this->formHelper = $formHelper;
  }

  public static function create(ContainerInterface $container) {
     return new static(
      $container->get('senapi_forms.form_helper')
     );
  }

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'senapi_forms_denuncia_form';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>Usted puede DENUNCIAR cualquier caso o irregularidad al SENAPI para que la institución pueda emitir o buscar la solución pertinente.<br>
                    Los campos marcados con asterisco (*) son OBLIGATORIOS.</p>'),
    ];

    $form['privacidad'] = [
      '#type' => 'radios',
      '#title' => $this->t('¿Solicita reserva de identidad?'),
      '#required' => TRUE,
      '#default_value' => 1,
      '#options' => [
        1 => t('SI'),
        0 => t('NO'),
      ],

      '#attributes' => ['class' => ['mb-3']],

      '#after_build' => [
        function ($element) {
          foreach (array_keys($element["#options"]) as $key) {
            if (isset($element[$key])) {
              $element[$key]['#attributes']['class'][] = 'custom-control-input';
              $element[$key]['#label_attributes']['class'][] = 'custom-control-label';
              $element[$key]['#wrapper_attributes']['class'] = 'custom-control custom-radio custom-control-inline';
            }
          }
          return $element;
        },
      ],
    ];

    $form['row_group'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'form-row',
        ],
      ],
    ];

    $form['row_group']['nombre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre y Apellido'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#wrapper_attributes' => ['class' => ['col-md-5']],
    ];

    $form['row_group']['tipo_documento'] = [
      '#type' => 'select',
      '#title' => $this->t('Tipo Documento Identidad'),
      '#required' => TRUE,
      '#options' => $this->formHelper->getDominio('tipo_documento'),
      '#attributes' => [
        'class' => ['form-control'],
      ],
      '#wrapper_attributes' => ['class' => ['col-md-3']],
    ];

    $form['row_group']['nro_documento'] = [
      '#type' => 'textfield',
      '#title' => $this->t(' N° Documento Identidad'),
      '#maxlength' => 15,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#wrapper_attributes' => ['class' => ['col-md-4']],
    ];

    $form['row_group2'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'form-row',
        ],
      ],
    ];

    $form['row_group2']['celular'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Número de celular'),
      '#size' => 60,
      '#maxlength' => 12,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#wrapper_attributes' => ['class' => ['col-md-4']],
    ];

    $form['row_group2']['telefono'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Número de teléfono'),
      '#size' => 60,
      '#maxlength' => 12,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#wrapper_attributes' => ['class' => ['col-md-4']],
    ];

    $form['row_group2']['correo'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#wrapper_attributes' => ['class' => ['col-md-4']],
    ];

    $form['row_group3'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'form-row',
        ],
      ],
    ];

    $form['row_group3']['direccion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dirección'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#wrapper_attributes' => ['class' => ['col-md-6']],
    ];

    $form['row_group3']['pais'] = [
      '#type' => 'select',
      '#title' => $this->t('País'),
      '#required' => TRUE,
      '#options' => $this->formHelper->getDominio('pais'),
      '#attributes' => [
        'class' => ['form-control'],
      ],
      '#wrapper_attributes' => ['class' => ['col-md-3']],
    ];

    $form['row_group3']['departamento'] = [
      '#type' => 'select',
      '#title' => $this->t('Departamento'),
      '#required' => FALSE,
      '#options' => $this->formHelper->getDominio('departamento'),
      '#attributes' => [
        'class' => ['form-control'],
      ],
      '#wrapper_attributes' => ['class' => ['col-md-3']],
      '#states' => [
        'visible' => [
          ':input[name="pais"]' => [
            ['value' => 'BO']
          ]
        ]
      ],
    ];

    $form['row_group4'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'form-row',
        ],
      ],
    ];

    $form['row_group4']['funcionario'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Servidor(es) público(s) denunciado(s)'),
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['form-control'],
        'autocomplete' => 'off',
        'placeholder' => $this->t('Nombre (s) - Área'),
      ],
      '#wrapper_attributes' => ['class' => ['col-md-6']],
    ];

    $form['row_group4']['descripcion'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Descripción concreta de la denuncia (Sea claro y conciso)'),
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['form-control'],
        'autocomplete' => 'off',
        'placeholder' => $this->t('¿Que ocurrio?, ¿Cuando?, Quien lo hizo, ¿Con quien?'),
      ],
      '#wrapper_attributes' => ['class' => ['col-md-6']],
    ];

    $form['row_group5'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'form-row',
        ],
      ],
    ];

    $form['row_group5']['indicio'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Indicios y /o pruebas'),
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['form-control'],
        'autocomplete' => 'off',
      ],
      '#wrapper_attributes' => ['class' => ['col-md-6']],
    ];

    $form['row_group5']['instancia'] = [
      '#type' => 'textarea',
      '#title' => $this->t('¿Denunció ante otras instancias dentro o fuera del SENAPI?, si es asi, indique que instancia'),
      #'#required' => TRUE,
      '#attributes' => [
        'class' => ['form-control'],
        'autocomplete' => 'off',
        'placeholder' => $this->t('¿Que instancia?, ¿Cuando?, ¿Se le dio respuesta? de ser así, indique cual fue.'),
      ],
      '#wrapper_attributes' => ['class' => ['col-md-6']],
    ];

    $form['row_group6'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'form-row',
        ],
      ],
    ];

    $form['row_group6']['fecha'] = [
      '#type' => 'date',
      '#title' => $this->t('Fecha de suceso/evento'),
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['form-control'],
        'type' => 'date',
      ],
      '#label_attributes' => [
        'class' => ['col-form-label'],
      ],
      '#wrapper_attributes' => ['class' => ['col-md-6']],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#attributes' => ['class' => ['ml-0 mr-0']],
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $pais = $form_state->getValue('pais');
    switch ($pais) {
      case 'BO':
        if (!in_array($form_state->getValue('departamento'), array_keys($this->formHelper->getDominio('departamento')))) {
          $form_state->setErrorByName('departamento', 'Departamento valor incorrecto.');
        }
        break;
      default:
        if (!in_array($pais, array_keys($this->formHelper->getDominio('pais')))) {
          $form_state->setErrorByName('pais', 'País valor incorrecto.');
        }
        break;
    }

    if (!in_array($form_state->getValue('tipo_documento'), array_keys($this->formHelper->getDominio('tipo_documento')))) {
      $form_state->setErrorByName('tipo_documento', 'Tipo de documento valor incorrecto..');
    }
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data = $form_state->getUserInput();

    foreach ($form_state->getCleanValueKeys() as $item) {
      unset($data[$item]);
    }

    $this->formHelper->parseFormData($data);


    $data['tipo_formulario'] = 'DENU';

    $connection = \Drupal::database();
    try {
      $id = $connection->insert('snp_transparencia')
        ->fields($data)
        ->execute();

      $tipoDocumento = $this->formHelper->getDominio('tipo_documento', [':codigo' => $data['tipo_documento']]);
      $data['tipo_documento'] = $tipoDocumento[$data['tipo_documento']];

      $pais = $this->formHelper->getDominio('pais', [':codigo' => $data['pais']]);
      $data['pais'] = $pais[$data['pais']];

      if (isset($data['departamento'])){
        $departamento = $this->formHelper->getDominio('departamento', [':codigo' => $data['departamento']]);
        $data['departamento'] = $departamento[$data['departamento']];
      }
      $tipoFormulario = $this->formHelper->getDominio('tipo_formulario', [':codigo' => $data['tipo_formulario']]);
      $data['formulario'] = $tipoFormulario[$data['tipo_formulario']];

      $data['privacidad'] = ($data['privacidad']) ? 'SI' : 'NO';
      $data['id'] = $id;

      $fecha = new \DateTime($data['fecha']);
      $data['fecha'] = \Drupal::service('date.formatter')
        ->format($fecha->getTimestamp(), $type = 'custom', $format = 'j \d\e F \d\e\l Y', $timezone = NULL, $langcode = 'es');

      $result = \Drupal::service('plugin.manager.mail')
        ->mail('swiftmailer', 'transparencia', $form_state->getValue(['correo']), \Drupal::languageManager()
          ->getDefaultLanguage()
          ->getId(), $params = ['data' => $data], $reply = NULL);
      if ($result['result']) {
        $connection
          ->update('snp_transparencia')
          ->fields([
            'usuario_estado' => 'ENV',
          ])
          ->condition('id', $id)
          ->execute();
      }

      $result = \Drupal::service('plugin.manager.mail')
        ->mail('swiftmailer', '_transparencia', 'transparencia@senapi.gob.bo', \Drupal::languageManager()
          ->getDefaultLanguage()
          ->getId(), $params = ['data' => $data], $reply = NULL);
      if ($result['result']) {
        $connection
          ->update('snp_transparencia')
          ->fields([
            'funcionario_estado' => 'ENV',
          ])
          ->condition('id', $id)
          ->execute();
      }

      \Drupal::messenger()
        ->addMessage("La acción ha sido completada satisfactoriamente, muchas gracias por usar este servicio.");
      #exit(0);
    } catch (\Exception $e) {
      switch ($e->getCode()) {
        case 23505:
          \Drupal::messenger()
            ->addWarning("La denuncia con el mensaje ha sido enviado.");
          break;
        default:
          \Drupal::messenger()
            ->addError("Error al guardar los datos, intente mas tarde.");
          break;
      }
    }
  }
}