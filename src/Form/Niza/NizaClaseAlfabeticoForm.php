<?php


namespace Drupal\senapi_forms\Form\Niza;


use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class NizaClaseAlfabeticoForm extends FormBase {

  public function getFormId() {
    return 'senapi_forms_niza_clase_alfabetico_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<h4>Lista de productos y servicios en orden alfabético</h4><p>Por favor seleccione una letra en el índice alfabético.</p>'),
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $session = Drupal::request()->getSession();
    $session->set('form', 'alfabetico');
  }

}
