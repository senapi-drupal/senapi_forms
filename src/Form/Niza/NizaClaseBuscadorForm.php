<?php


namespace Drupal\senapi_forms\Form\Niza;


use Drupal;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class NizaClaseBuscadorForm extends FormBase {

  public function getFormId() {
    return 'senapi_forms_niza_clase_buscador_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'][] = 'p-3';
    $form['niza'] = [
      '#prefix' => '<div class="row no-padding">',
      '#suffix' => '</div>',
    ];

    $form['niza']['col'] = [
      '#prefix' => '<div class="col-md-12 needs-validation">',
      '#suffix' => '</div>',
    ];

    $form['niza']['col']['buscador'] = [
      '#prefix' => '<div class="input-group">',
      '#suffix' => '</div>',
    ];

    $form['niza']['col']['buscador']['termino'] = [
      '#type' => 'textfield',
      '#theme_wrappers' => [],
      '#required' => TRUE,
      '#value' => '',
      '#attributes' => [
        'placeholder' => 'Término de búsqueda',
      ],
    ];

    $form['niza']['col']['buscador']['btn'] = [
      '#type' => 'button',
      '#value' => 'Buscar',
      '#prefix' => '<div class="input-group-append">',
      '#suffix' => '</div><div class="invalid-tooltip">Campo requerido</div>',
      '#ajax' => [
        'callback' => [$this, 'ajaxFormNizaClaseCallback'],
        'wrapper' => 'senapi-forms-buscador-wrapper',
        'event' => 'click',
        'effect' => 'fade',
        'progress' => [
          'type' => 'nizabuscador',
          'message' => NULL,
        ],
      ],
    ];

    /* $form['niza']['col']['operador'] = [
       '#type' => 'radios',
       '#title' => $this->t('Operador:'),
       '#required' => TRUE,
       '#default_value' => 'OR',
       '#options' => [
         'OR' => 'OR',
         'AND' => 'AND',
       ],
       '#attributes' => ['class' => ['mb-3']],

       '#after_build' => ['custom_process_radios'],
     ];*/

    $form['niza']['col']['clases'] = [
      '#type' => 'radios',
      '#title' => $this->t('Seleccione clases:'),
      '#theme_wrappers' => [],
      '#required' => TRUE,
      '#default_value' => 'ALL',
      '#options' => [
        'PROD' => 'Productos',
        'SERV' => 'Servicios',
        'ALL' => 'Todas',
        'NONE' => 'Ninguna',
      ],
      '#attributes' => ['class' => ['mb-3']],
      '#after_build' => ['custom_process_radios'],
      '#theme_type' => 'foobar',
    ];

    $options = [];
    $products = $services = [];
    foreach (range(1, 34) as $item) {
      $products += [$item => $item];
    }
    foreach (range(35, 45) as $item) {
      $services += [$item => $item];
    }
    $options += $products + $services;
    $form['niza']['col']['indice'] = [
      '#type' => 'checkboxes',
      '#default_value' => [],
      '#options' => $options,
      '#description' => '',
      '#attributes' => ['class' => ['mb-3']],
      '#after_build' => ['custom_process_checkboxes'],
      '#prefix' => '<div class="" id="senapi-forms-clases-wrapper">',
      '#suffix' => '</div>',
    ];


    $form['niza']['col']['resultado-buscador'] = [
      '#type' => 'container',
      '#prefix' => '<hr/><div class="row no-padding mt-3" id="senapi-forms-buscador-wrapper"><div class="col-md-12">',
      '#suffix' => '</div></div>',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.

    $session = Drupal::request()->getSession();
    $session->set('form', 'buscador');
  }

  public function ajaxFormNizaClaseCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new InvokeCommand('.input-group', 'addClass', ['was-validated']));

    if ($form_state->getValue('termino') !== FALSE) {
      $termino = Drupal::request()->request->get('termino');
      $indice = Drupal::request()->request->get('indice');

      if ($indice == NULL) {
        $response->addCommand(new HtmlCommand('#edit-resultado-buscador', t('<div class="alert alert-danger" role="alert">@message</div>', ['@message' => 'Por favor, seleccione al menos una clase en la que buscar.'])));
        return $response;
      }

      if (empty($termino)) {
        $response->addCommand(new HtmlCommand('.invalid-tooltip', 'Por favor, proporcione términos de búsqueda.'));
        $response->addCommand(new HtmlCommand('#edit-resultado-buscador', t('<div class="alert alert-danger" role="alert">@message</div>', ['@message' => 'Por favor, proporcione términos de búsqueda.'])));
        return $response;
      }

      $connection = Drupal::database();
      $sql = "SELECT clase, descripcion, explicacion FROM snp_niza_clase where clase in(:indice[]) order by clase";
      $result = $connection->query($sql, [':indice[]' => array_values($indice)])
        ->fetchAll();

      $pattern = Drupal::service('pathauto.alias_cleaner')
        ->cleanString(preg_replace('/\s+/', '', $termino));

      $params = [
        ':terminoa' => $pattern . '%',
        ':terminob' => $pattern . '*%',
        ':terminoc' => $pattern . ' [%',
        ':aterminoz' => '% ' . $pattern . '%',
        ':terminoz' => '% ' . $pattern,
        ':termino' => $pattern,
      ];

      $plSql = "WITH 
                pl as (
                  SELECT clase, nro, palabra, significado FROM snp_niza_pal
                  where (UNACCENT(significado) ilike :terminoa 
                  or UNACCENT(significado) ilike :terminob 
                  or UNACCENT(significado) ilike :terminoc 
                  or UNACCENT(significado) ilike :termino
                  or UNACCENT(significado) ilike :aterminoz 
                  or UNACCENT(significado) ilike :terminoz) 
                ),
                cl as (
                  SELECT clase, descripcion, explicacion FROM snp_niza_clase
                  where (UNACCENT(descripcion) ilike :terminoa 
                  or UNACCENT(descripcion) ilike :aterminoz  
                  or UNACCENT(descripcion) ilike :terminoz  
                  or UNACCENT(explicacion) ilike :terminoa 
                  or UNACCENT(explicacion) ilike :aterminoz  
                  or UNACCENT(explicacion) ilike :terminoz)                 
                ),
                plcl as (
                 select 
                 pl.clase, pl.nro, pl.palabra, pl.significado,
                 cl.descripcion, cl.explicacion  
                 from pl 
                 left join cl on pl.clase = cl.clase
                ),
                clpl as (
                  select 
                 cl.clase,  pl.nro, pl.palabra, pl.significado,
                  cl.descripcion, cl.explicacion 
                  from pl 
                  right join cl on pl.clase = cl.clase
                ),
                cllpll as (
                  select  distinct * 
                  from plcl
                  union 
                  select distinct * from clpl
                )
                select 
                  cllpll.clase, cllpll.nro, cllpll.palabra, cllpll.significado,
                  snc.descripcion, snc.explicacion 
                from 
                snp_niza_clase snc
                inner join cllpll on snc.clase = cllpll.clase
                ORDER BY
                  (case when cllpll.significado ilike :termino then 1 else 0 end) desc,
                  (case when cllpll.significado ilike :terminob then 1 else 0 end) desc,
                  (case when cllpll.significado ilike :terminoc then 1 else 0 end) desc,
                  (case when cllpll.significado ilike :terminoa then 1 else 0 end) desc,
                  (case when cllpll.significado ilike :aterminoz then 1 else 0 end) desc,
                  (case when cllpll.significado ilike :terminoz then 1 else 0 end) desc
                ";

      $plResult = $connection->query($plSql, $params)->fetchAll();

      $items = [];
      foreach ($plResult as $record) {
        $items[$record->clase]['descripcion'] = $record->descripcion;
        $items[$record->clase]['explicacion'] = $record->explicacion;
        $items[$record->clase]['clase'] = $record->clase;

        if (!is_null($record->nro) && !is_null($record->palabra) && !is_null($record->significado)) {
          $items[$record->clase]['items'][] = [
            'nro' => $record->nro,
            'palabra' => $record->palabra,
            'significado' => $record->significado,
          ];
        }
      }

      $cards = [];
      foreach ($items as $key => $record) {

        $currents = [];
        if (isset($record['items'])) {
          $currents = $record['items'];
        }

        $tr = [];
        foreach ($currents as $current) {
          $alias = $current['significado'];
          $tr[] = t(
            '
                <tr>
                    <th scope="row" class="p-2" style="min-width: 100px"><a href="http://tmclass.tmdn.org/ec2/search/find?language=es&text=@tmclass&niceClass=&size=25&page=1&officeList=ES" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i> @nro</a></th>
                    <td class="p-2">@significado</td>
                </tr>',
            [
              '@nro' => $current['nro'],
              '@tmclass' => $current['significado'],
              '@significado' => t(preg_replace("/($pattern)/i", '<mark class="text-white bg-dark p-0">$1</mark>', $alias)),
            ]
          );
        }

        if (count($tr) > 0) {
          $table = t('
              <table class="table">
              <tbody>
              @tr
              </tbody>
              </table>', ['@tr' => t(join('', $tr))]);
        }
        else {
          $table = '';
        }

        $cards[] = t('
              <div class="card mb-2 wrapper-accordion-snp0@id">
                    <div class="card-accordion card-header" id="heading-snp0@id"><a aria-controls="collapse-snp0@id" aria-expanded="@first" class="btn btn-link d-block text-left" data-target="#collapse-snp0@id" data-toggle="collapse"> <span class="icon-collapse"></span> <strong class="mb-0"> CLASE @clase </strong> </a></div>
                    <div aria-labelledby="heading-snp0@id" class="collapse @collapse" data-parent=".wrapper-accordion-snp0@id" id="collapse-snp0@id">
                          <div class="card-body">
                            <p>@descripcion</p>
                            <div class="card mb-2 wrapper-accordion-snp0@id@id@id@id">
                                <div class="card-accordion card-header" id="heading-snp0@id@id@id@id"><a aria-controls="collapse-snp0@id@id@id@id" aria-expanded="false" class="btn btn-link d-block text-left" data-target="#collapse-snp0@id@id@id@id" data-toggle="collapse"> <span class="icon-collapse"></span> <strong class="mb-0"> Nota explicativa </strong> </a></div>
                                <div aria-labelledby="heading-snp0@id@id@id@id" class="collapse" data-parent=".wrapper-accordion-snp0@id@id@id@id" id="collapse-snp0@id@id@id@id">
                                    <div class="card-body p-1">
                                        @explicacion        
                                    </div>
                                </div>
                            </div>
                            @table
                          </div>
                    </div>
                </div>
              ', [
          '@id' => $record['clase'],
          '@first' => 'true',
          '@collapse' => 'show',
          '@clase' => $record['clase'],
          '@table' => $table,
          '@descripcion' => t(preg_replace("/($pattern)/i", '<mark class="text-white bg-dark p-0">$1</mark>', str_replace(';', "<span class='font-weight-bold'>;</span>", $record['descripcion']))),
          '@explicacion' => t(preg_replace("/($pattern)/i", '<mark class="text-white bg-dark p-0">$1</mark>', $record['explicacion'])),
        ]);
      }
      if (count($cards) > 0) {
        $response->addCommand(
          new HtmlCommand('#edit-resultado-buscador', t('@cards', ['@cards' => t(join('', $cards))]))
        );
      }
      else {
        $response->addCommand(
          new HtmlCommand('#edit-resultado-buscador', t('<div class="alert alert-info" role="alert">@message</div>', ['@message' => 'El término buscado ' . $termino . ' no existe en la(s) clase(s) escogida(s).']))
        );
      }
    }

    return $response;
  }

}
