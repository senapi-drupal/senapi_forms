<?php


namespace Drupal\senapi_forms\Form\Niza;


use Drupal;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class NizaClaseIndiceForm extends FormBase {

  public function getFormId() {
    return 'senapi_forms_niza_clase_indice_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attributes']['class'][] = 'p-3';

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t(''),
    ];


    $form['niza'] = [
      '#prefix' => '<div class="row no-padding">',
      '#suffix' => '</div>',
    ];
    $form['niza']['col1'] = [
      '#prefix' => '<div class="col-md-3">',
      '#suffix' => '</div>',
    ];

    $form['niza']['col1']['clase'] = [
      '#type' => 'container',
      '#markup' => $this->t('<h4 class="pl-3 pr-3">Indice de Clases</h4>'),
      '#prefix' => '<div class="row no-padding">',
      '#suffix' => '</div>',
    ];

    $form['niza']['col1']['producto'] = [
      '#type' => 'markup',
      '#prefix' => '<div class="card mb-2 wrapper-accordion-snp-0101010101">
                    <div class="card-accordion card-header" id="heading-snp-0101010101"><a aria-controls="collapse-snp-0101010101" aria-expanded="true" class="btn btn-link d-block text-left" data-target="#collapse-snp-0101010101" data-toggle="collapse"> <span class="icon-collapse"></span> <strong class="mb-0"> Productos </strong> </a></div>
                    <div aria-labelledby="heading-snp-0101010101" class="collapse show" data-parent=".wrapper-accordion-snp-0101010101" id="collapse-snp-0101010101">
                          <div class="card-body p-1"><div class="row no-padding">',
      '#suffix' => '</div></div></div></div>',
    ];

    foreach (range(1, 34) as $item) {
      $form['niza']['col1']['producto'][$item] = [
        '#title' => $item,
        '#type' => 'button',
        '#value' => $item,
        '#default_value' => $item,
        '#attributes' => ['class' => ['mb-2 btn-sm']],
        '#prefix' => '<div class="col-2 col-sm-1 col-md-3 col-lg-2 text-center">',
        '#suffix' => '</div>',
        '#ajax' => [
          'callback' => [$this, 'ajaxFormNizaClaseCallback'],
          'wrapper' => 'senapi-forms-wrapper',
          'event' => 'click',
          'effect' => 'fade',
          'progress' => [
            'type' => 'nizaindice',
            'message' => NULL,
          ],
        ],
      ];
    }

    $form['niza']['col1']['servicio'] = [
      '#type' => 'markup',
      '#prefix' => '<div class="card mb-2 wrapper-accordion-snp-01010102">
                    <div class="card-accordion card-header" id="heading-snp-01010102"><a aria-controls="collapse-snp-01010102" aria-expanded="true" class="btn btn-link d-block text-left" data-target="#collapse-snp-01010102" data-toggle="collapse"> <span class="icon-collapse"></span> <strong class="mb-0"> Servicios </strong> </a></div>
                    <div aria-labelledby="heading-snp-01010102" class="collapse show" data-parent=".wrapper-accordion-snp-01010102" id="collapse-snp-01010102">
                          <div class="card-body p-1"><div class="row no-padding">',
      '#suffix' => '</div></div></div></div>',
    ];
    foreach (range(35, 45) as $item) {
      $form['niza']['col1']['servicio'][$item] = [
        '#title' => $item,
        '#type' => 'button',
        '#value' => $item,
        '#default_value' => $item,
        '#attributes' => [
          'class' => ['mb-2 btn-sm'],
        ],
        '#prefix' => '<div class="col-2 col-sm-1 col-md-3 col-lg-2 text-center">',
        '#suffix' => '</div>',
        '#ajax' => [
          'callback' => [$this, 'ajaxFormNizaClaseCallback'],
          'wrapper' => 'senapi-forms-wrapper',
          'event' => 'click',
          'effect' => 'fade',
          'progress' => [
            'type' => 'nizaindice',
            'message' => NULL,
          ],
        ],
      ];
    }

    $form['niza']['col2'] = [
      '#prefix' => '<div class="col-md-9">',
      '#suffix' => '</div>',
    ];

    $form['niza']['col2']['resultado'] = [
      '#type' => 'container',
      '#prefix' => '<h4 class="pl-3 pr-3">Lista de productos y servicios ordenados por clase</h4><div class="row no-padding"><div class="col-md-12" id="senapi-forms-wrapper"><p class="pl-3 pr-3">Por favor seleccione una clase en el índice de clases.</p>',
      '#suffix' => '</div></div>',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $session = Drupal::request()->getSession();
    $session->set('form', 'indice');
  }

  public function ajaxFormNizaClaseCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state->getValue('op') !== FALSE) {
      $op = $form_state->getValue('op');

      $connection = Drupal::database();
      $sql = 'SELECT * FROM snp_niza_clase where clase = :clase order by clase';
      $result = $connection->query($sql, [':clase' => $op])->fetchAll();

      $cards = [];
      foreach ($result as $key => $record) {
        $sql = 'SELECT * FROM snp_niza_pal where clase = :clase order by nro';
        $plResult = $connection->query($sql, [':clase' => $op])->fetchAll();

        $currents = [];
        if (count($plResult) > 0) {
          $currents = $plResult;
        }
        $tr = [];
        foreach ($currents as $current) {
          $tr[] = t(
            '
                <tr>
                    <th  scope="row" class="p-2" style="min-width: 105px"><a href="http://tmclass.tmdn.org/ec2/search/find?language=es&text=@tmclass&niceClass=&size=25&page=1&officeList=ES" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i> @nro</a></th>
                    <td class="p-2">@significado</td>
                </tr>',
            [
              '@nro' => $current->nro,
              '@tmclass' => $current->significado,
              '@significado' => t(str_replace($current->palabra, "<mark class='font-weight-bold'>$current->palabra</mark>", $current->significado)),
            ]
          );
        }

        $table = t('
            <div class="table-responsive">
              <table class="table">
              <tbody>
              @tr
              </tbody>
              </table></div>', ['@tr' => t(join('', $tr))]);

        $cards[] = t('
              <div class="card mb-2 wrapper-accordion-snp0@id">
                    <div class="card-accordion card-header" id="heading-snp0@id"><a aria-controls="collapse-snp0@id" aria-expanded="@first" class="btn btn-link d-block text-left" data-target="#collapse-snp0@id" data-toggle="collapse"> <span class="icon-collapse"></span> <strong class="mb-0"> CLASE @clase </strong> </a></div>
                    <div aria-labelledby="heading-snp0@id" class="collapse @collapse" data-parent=".wrapper-accordion-snp0@id" id="collapse-snp0@id">
                          <div class="card-body p-3">
                            <p>@descripcion</p>                            
                            <div class="card mb-2 wrapper-accordion-snp0@id@id@id">
                                <div class="card-accordion card-header" id="heading-snp0@id@id@id"><a aria-controls="collapse-snp0@id@id@id" aria-expanded="false" class="btn btn-link d-block text-left" data-target="#collapse-snp0@id@id@id" data-toggle="collapse"> <span class="icon-collapse"></span> <strong class="mb-0"> Nota explicativa </strong> </a></div>
                                <div aria-labelledby="heading-snp0@id@id@id" class="collapse" data-parent=".wrapper-accordion-snp0@id@id@id" id="collapse-snp0@id@id@id">
                                    <div class="card-body p-1">
                                        @explicacion        
                                    </div>
                                </div>
                            </div>
                            @table
                          </div>
                    </div>
                </div>
              ', [
          '@id' => $record->clase,
          '@first' => 'true',
          '@collapse' => 'show',
          '@clase' => $record->clase,
          '@table' => $table,
          '@descripcion' => t(str_replace(';', "<span class='font-weight-bold'>;</span>", $record->descripcion)),
          '@explicacion' => t($record->explicacion),
        ]);
      }

      if (count($cards) > 0) {
        $response->addCommand(
          new HtmlCommand('#senapi-forms-wrapper', t('@cards', ['@cards' => t(join('', $cards))]))
        );
      }
      else {
        $response->addCommand(
          new HtmlCommand('#senapi-forms-wrapper', t('<div class="alert alert-info" role="alert">@message</div>', ['@message' => 'No existe la clase escogida.']))
        );
      }
    }


    return $response;
  }

}
