<?php


namespace Drupal\senapi_forms\Form\Niza;


use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class NizaClaseTitulosForm extends FormBase {

  public function getFormId() {
    return 'senapi_forms_niza_clase_titulos_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<h4>Lista de clases con notas explicativas </h4>'),
    ];

    $connection = Drupal::database();
    $sql = 'SELECT * FROM snp_niza_clase order by clase';
    $result = $connection->query($sql)->fetchAll();

    foreach ($result as $key => $record) {
      $form['#attributes']['class'][] = 'p-3';
      $form['description'][$key] = [
        '#type' => 'markup',
        '#markup' => $this->t('
                <div class="card mb-2 wrapper-accordion-snp0@id">
                    <div class="card-accordion card-header" id="heading-snp0@id"><a aria-controls="collapse-snp0@id" aria-expanded="@first" class="btn btn-link d-block text-left" data-target="#collapse-snp0@id" data-toggle="collapse"> <span class="icon-collapse"></span> <strong class="mb-0"> CLASE @clase </strong> </a></div>
                    <div aria-labelledby="heading-snp0@id" class="collapse @collapse" data-parent=".wrapper-accordion-snp0@id" id="collapse-snp0@id">
                          <div class="card-body">
                            <p>@descripcion</p>
                            <h5>Nota explicativa</h5>
                            @explicacion
                          </div>
                    </div>
                </div>', [
          '@id' => $key,
          '@first' => ($key == 0) ? 'true' : 'false',
          '@collapse' => ($key == 0) ? 'show' : '',
          '@clase' => $record->clase,
          '@descripcion' => t(str_replace(';', "<span class='font-weight-bold'>;</span>", $record->descripcion)),
          '@explicacion' => t($record->explicacion),
        ]),
      ];
    }
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $session = Drupal::request()->getSession();
    $session->set('form', 'titulos');
  }
}
