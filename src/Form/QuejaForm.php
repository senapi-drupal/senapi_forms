<?php
/**
 * Created by PhpStorm.
 * User: senapi
 * Date: 16-10-18
 * Time: 03:02 PM
 */

namespace Drupal\senapi_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class QuejaForm extends FormBase {

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'senapi_forms_queja_form';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>Usted puede QUEJARSE de cualquier caso o irregularidad al SENAPI para que esta institución pueda emitir o buscar alguna solución pertinente.<br>
                    Los campos marcados con asterisco (*) son OBLIGATORIOS.</p>'),
    ];

    $form['people_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre y Apellido'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];

    $form['row_group'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'form-row',
        ],
      ],
    ];

    $form['row_group']['people_type_identity'] = [
      '#type' => 'select',
      '#title' => $this->t('Tipo Documento Identidad'),
      '#required' => TRUE,
      '#options' => [
        '' => $this->t('-- Seleccione --'),
        '1' => $this->t('C.I.'),
        '2' => $this->t('R.U.N.'),
        '3' => $this->t('Pasaporte'),
        '4' => $this->t('N.I.T.'),
        '5' => $this->t('D.N.I.'),
        '6' => $this->t('Otro'),
      ],
      '#attributes' => [
        'class' => ['form-control'],
      ],
      '#wrapper_attributes' => ['class' => ['col-md-4']],
    ];

    $form['row_group']['people_identity'] = [
      '#type' => 'textfield',
      '#title' => $this->t(' N° Documento Identidad'),
      '#size' => 60,
      '#maxlength' => 15,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
        'class' => ['form-control'],
      ],
      '#wrapper_attributes' => ['class' => ['col-md-4']],
    ];


    $form['row_group']['people_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#wrapper_attributes' => ['class' => ['col-md-4']],
    ];

    $form['people_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Queja'),
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::messenger()->addMessage("Datos recibido exitosamente.");
    $email = $form_state->getValue('people_email');

    $session = \Drupal::request()->getSession();
    $session->set('form', 'queja');
  }
}