<?php
/**
 * Created by PhpStorm.
 * User: senapi
 * Date: 16-10-18
 * Time: 03:03 PM
 */

namespace Drupal\senapi_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class SugerenciaForm extends FormBase {

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'senapi_forms_sugerencia_form';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>Usted puede SUGERIR al SENAPI algún tema de interes general.<br>
                    Los campos marcados con asterisco (*) son OBLIGATORIOS.</p>'),
    ];

    $form['people_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre y Apellido'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];

    $form['people_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];

    $form['people_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Sugerencia'),
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::messenger()->addMessage("Datos recibido exitosamente.");
    $email = $form_state->getValue('people_email');

    $session = \Drupal::request()->getSession();
    $session->set('form', 'sugerencia');
  }
}