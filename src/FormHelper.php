<?php
/**
 * Class FormHelper
 */

namespace Drupal\senapi_forms;

class FormHelper {

  public function getDominio($dominio, $params = []) {
    $connection = \Drupal::database();
    $sql = 'SELECT * FROM snp_dominio WHERE dominio = :dominio';

    $result = [];
    switch ($dominio) {
      case 'tipo_documento':
      case 'pais':
      case 'departamento':
      case 'tipo_formulario':
        if (is_array($params)) {
          foreach ($params as $key => $value) {
            list($prefix, $column) = explode(':', $key);
            $sql .= ' AND ' . $column . ' = ' . $key;
          }

          $params += [':dominio' => $dominio];
          $result = $connection
            ->query($sql, $params)
            ->fetchAllKeyed(2, 3);
        }
        break;
    }

    return $result;
  }

  public function parseFormData(array &$data = []) {
    if (isset($data['pais']) && $data['pais'] != 'BO') {
      unset($data['departamento']);
    }

    foreach ($data as $key => $item) {
      if (empty($item) && in_array($key, ['telefono', 'instancia'])) {
        unset($data[$key]);
        continue;
      }

      if (is_string($item)) {
        $data[$key] = join(' ', array_filter(array_map('trim', explode(' ', $item))));
        if (in_array($key, [
          'nombre',
          'descripcion',
          'funcionario',
          'direccion',
          'indicio',
          'instancia',
        ])) {
          $data[$key] = mb_strtoupper($data[$key]);
        }

        if (in_array($key, ['correo'])) {
          $data[$key] = mb_strtolower($data[$key]);
        }
      }
    }
  }
}