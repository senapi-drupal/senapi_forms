<?php


namespace Drupal\senapi_forms\Plugin\Block;


use Drupal\Core\Block\BlockBase;

/**
 * Class ContactoFormBlock
 *
 * @Block(
 *   id = "form_contacto_block",
 *   admin_label = @Translation("Formulario de contacto")
 * )
 */
class ContactoFormBlock extends BlockBase{

  public function build() {
    return \Drupal::formBuilder()
      ->getForm('Drupal\senapi_forms\Form\ContactoForm');
  }

}