<?php

namespace Drupal\senapi_forms\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class DenunciaFormBlock
 *
 * @Block(
 *   id = "form_denuncia_block",
 *   admin_label = @Translation("Formulario de denunicia")
 * )
 */
class DenunciaFormBlock extends BlockBase {

  public function build() {
    return \Drupal::formBuilder()
      ->getForm('Drupal\senapi_forms\Form\DenunciaForm');
  }

}