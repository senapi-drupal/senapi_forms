<?php


namespace Drupal\senapi_forms\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class NizaFormBlock
 *
 * @Block(
 *   id = "form_niza_block",
 *   admin_label = @Translation("Buscador de Clase Niza")
 * )
 */
class NizaFormBlock extends BlockBase {

  public function build() {
    $session = \Drupal::request()->getSession();
    $formName = 'buscador';
    if ($session->get('form')) {
      $formName = $session->get('form');
      $session->remove('form');
    }

    return [
      '#attached' => [
        'library' => ['senapi_forms/niza-buscador']
      ],
      '#type' => 'inline_template',
      '#template' => '
         <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
            {% for tab in tabs %}
               <a class="nav-item nav-link {{ tab.active }}" id="nav-{{ tab.key }}-tab" data-toggle="tab" href="#nav-{{ tab.key }}" role="tab" aria-controls="nav-{{ tab.key }}" aria-selected="{{ (tab.active is empty)? \'false\' : \'true\' }}">{{ tab.name }}</a>
            {% endfor %}
            </div>
         </nav>
         <div class="tab-content" id="nav-tabContent">
             {% for tab in tabs %}
             <div class="tab-pane fade {{ tab.active }} bg-white" id="nav-{{ tab.key }}" role="tabpanel" aria-labelledby="nav-{{ tab.key }}-tab">{{ tab.content }}</div>
             {% endfor %}
         </div>',
      '#context' => [
        'tabs' => [
          [
            'key' => 'buscador',
            'name' => 'Buscar',
            'content' => $this->renderFormBuilder('Drupal\senapi_forms\Form\Niza\NizaClaseBuscadorForm'),
            'active' => ($formName == 'buscador')? 'show active' : '',
          ],
          [
            'key' => 'clases',
            'name' => 'Clases',
            'content' => $this->renderFormBuilder('Drupal\senapi_forms\Form\Niza\NizaClaseIndiceForm'),
            'active' => ($formName == 'indice')? 'show active' : '',
          ],
          /*[
            'key' => 'alfabetico',
            'name' => 'Alfabético',
            'content' => $this->renderFormBuilder('Drupal\senapi_forms\Form\Niza\NizaClaseAlfabeticoForm'),
            'active' => ($formName == 'alfabetico')? 'show active' : '',
          ],*/
          [
            'key' => 'titulo-de-clase',
            'name' => 'Títulos de clase',
            'content' => $this->renderFormBuilder('Drupal\senapi_forms\Form\Niza\NizaClaseTitulosForm'),
            'active' => ($formName == 'titulos')? 'show active' : '',
          ],

        ],
      ],
    ];
  }

  protected function renderFormBuilder($formNamespace) {
     $form = \Drupal::formBuilder()
      ->getForm($formNamespace);
      return \Drupal::service('renderer')->render($form);
  }
}
