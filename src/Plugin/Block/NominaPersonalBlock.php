<?php


namespace Drupal\senapi_forms\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class NominaPersonalBlock
 *
 * @Block(
 *   id = "nomina_personal_block",
 *   admin_label = @Translation("Nómina de Personal Dependiente")
 * )
 */
class NominaPersonalBlock extends BlockBase {
  public function build() {
    $connection = \Drupal::database();
      $sql = "SELECT p.nombre, p.paterno, p.materno, p.cargo, 
                d.nombre as tipo, 
                d2.nombre as direccion,
                d3.nombre as rol 
                FROM snp_personal p
                LEFT JOIN snp_dominio d ON p.tipo_personal = d.codigo and d.dominio = 'tipo_personal'
                LEFT JOIN snp_dominio d2 ON p.direccion_personal = d2.codigo and d2.dominio = 'direccion_personal'
                LEFT JOIN snp_dominio d3 ON p.rol_personal = d3.codigo and d3.dominio = 'rol_personal'
                WHERE p.rol_personal IN('RESP', 'PERS') ORDER BY d2.orden";
      $result = $connection
              ->query($sql)
              ->fetchAll();

      $data = [];
      foreach ($result as $item) {
          $data[$item->tipo][$item->direccion][]  = $item;
      }

     return [
      '#type' => 'inline_template',
       '#theme' => 'block_personal',
       '#data' => $data,
    ];
  }
}