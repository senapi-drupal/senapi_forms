<?php


namespace Drupal\senapi_forms\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class NotificationBlock
 *
 * @Block(
 *   id  = "notification_block",
 *   admin_label = @Translation("Tablero Virtual de Notificaciones
 *   Electrónicas")
 * )
 */
class NotificationBlock extends BlockBase {

  public function build() {
    return [
      '#attached' => [
        'library' => ['senapi_uiv0/datatables', 'senapi_forms/notification'],
      ],
      '#type' => 'inline_template',
      '#template' => '
         <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
            {% for tab in tabs %}
               <a class="nav-item nav-link {{ tab.active }}" id="nav-{{ tab.key }}-tab" data-toggle="tab" href="#nav-{{ tab.key }}" role="tab" aria-controls="nav-{{ tab.key }}" aria-selected="{{ (tab.active is empty)? \'false\' : \'true\' }}">{{ tab.name }}</a>
            {% endfor %}
            </div>
         </nav>
         <div class="tab-content" id="nav-tabContent">
             {% for tab in tabs %}
             <div class="tab-pane fade {{ tab.active }} bg-white p-3" id="nav-{{ tab.key }}" role="tabpanel" aria-labelledby="nav-{{ tab.key }}-tab">{{ tab.content }}</div>
             {% endfor %}
         </div>',
      '#context' => [
        'tabs' => [
          [
            'key' => 'buscador',
            'name' => 'Busqueda',
            'content' => t('
                                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                      <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                          <div class="alert alert-warning alert-dismissible m-0 fade show" role="alert">
                                              <strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong>
                                              Para obtener más información usted deberá registrarse en el sistema SIPI haciendo click <a href="http://devsipi.senapi.gob.bo/validate-registration" target="_blank">AQUÍ</a>.
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                          </div>
                                          
                                        </div>
                                      </div>
                                    </div>
                                    <table id="notification" class="table table-striped table-bordered table-hover"></table>'),
            'active' => (TRUE) ? 'show active' : '',
          ],
        ],
      ],
    ];
  }

}
