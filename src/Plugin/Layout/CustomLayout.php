<?php

namespace Drupal\senapi_forms\Plugin\Layout;


use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomLayout extends LayoutDefault implements PluginFormInterface, ContainerFactoryPluginInterface {

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
        'title_text' => '',
        'title_element' => '',
        'title_classes' => '',
      ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $form['title_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $configuration['title_text'],
      '#description' => $this->t('Custom title for this section.'),
    ];

    $options = [
      'span' => $this->t('Span'),
      'div' => $this->t('Div'),
      'h1' => $this->t('Heading 1'),
      'h2' => $this->t('Heading 2'),
      'h3' => $this->t('Heading 3'),
      'h4' => $this->t('Heading 4'),
    ];

    $form['title_element'] = [
      '#type' => 'select',
      '#title' => $this->t('Title element'),
      '#default_value' => !empty($configuration['title_element']) ? $configuration['title_element'] : 'h2',
      '#options' => $options,
      '#description' => $this->t('Element for the custom title.'),
      '#states' => [
        'visible' => [
          ':input[name="layout_settings[title_text]"]' => ['filled' => TRUE],
        ],
      ],
    ];

    $form['title_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title classes'),
      '#default_value' => $configuration['title_classes'],
      '#description' => $this->t('Custom class css.'),
      '#states' => [
        'visible' => [
          ':input[name="layout_settings[title_text]"]' => ['filled' => TRUE],
        ],
      ],
    ];


    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement validateConfigurationForm() method.
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['title_text'] = $form_state->getValue('title_text');
    $this->configuration['title_element'] = $form_state->getValue('title_element');
    $this->configuration['title_classes'] = $form_state->getValue('title_classes');
  }

  /**
   * {@inheritDoc}
   */
  public function build(array $regions) {
    return  parent::build($regions);
  }


}