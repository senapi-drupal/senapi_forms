<?php
namespace Drupal\senapi_forms\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritDoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    if ($route = $collection->get('user.login')) {
      $route->setRequirement('_custom_access' , 'Drupal\senapi_forms\Access\CustomAccessCheck::access');
    }
    if ($route = $collection->get('user.pass')) {
      $route->setRequirements(['_custom_access' => 'Drupal\senapi_forms\Access\CustomAccessCheck::access']);
    }
  }
}